import React from 'react';
import ReactDOM from 'react-dom';

import {
  Appear,
  Box,
  CodePane,
  CodeSpan,
  Deck,
  FlexBox,
  FullScreen,
  Grid,
  Heading,
  Image as SpectacleImage,
  Link,
  ListItem,
  Markdown,
  Notes,
  OrderedList,
  Progress,
  Slide,
  SpectacleLogo,
  Stepper,
  Text,
  UnorderedList,
  indentNormalizer,
  Quote,
  Table,
  TableBody,
  TableRow,
  TableCell
} from 'spectacle';

// SPECTACLE_CLI_THEME_START
const theme = {
  fonts: {
    header: '"Lato", Helvetica, Arial, sans-serif',
    text: '"Lato", Helvetica, Arial, sans-serif'
  },
  colors: {
    primary: "#ffffff",
    secondary: "#ffffff",
    tertiary: "#000000",
  },
  fontSizes: {
    header: "64px",
    paragraph: "28px",
  },
};
// SPECTACLE_CLI_THEME_END

// SPECTACLE_CLI_TEMPLATE_START
const template = () => (
  <FlexBox
    justifyContent="space-between"
    position="absolute"
    bottom={0}
    width={1}
  >
    <Box padding="0 1em">
      <FullScreen />
    </Box>
    <Box padding="1em">
      <Progress />
    </Box>
  </FlexBox>
);
// SPECTACLE_CLI_TEMPLATE_END

const Image = (props) => {
  const width = 1300;
  const height = 500;
  return <SpectacleImage width={width} height={height} maxWidth={width} maxHeight={height} {...props} />
};

const Presentation = () => (
  <Deck theme={theme} template={template} transitionEffect="none">
    <Slide></Slide>
    <Slide>
      <FlexBox height="100%">
        <Box>
          <Heading>David Alan Hjelle</Heading>
          <Text textAlign="center">
            <Link href="mailto:dahjelle@thehjellejar.com">
              dahjelle@thehjellejar.com
            </Link>
          </Text>
          <Text textAlign="center">
            <Link href="https://iconcmo.com">https://iconcmo.com</Link>
          </Text>
        </Box>
        <Image src="images/family.jpeg" maxWidth="40vw" overflow="hidden" width="50%" />
      </FlexBox>
    </Slide>
    <Slide>
      <Link href="https://www.amazon.com/Effective-Engineer-Engineering-Disproportionate-Meaningful/dp/0996128107/ref=sr_1_1?dchild=1&keywords=the+effective+engineer&qid=1611690828&sr=8-1" target="_blank" style={{display: "flex", justifyContent: 'center'}}>
        <Image height={650} maxHeight={650} src="images/book.png"  />
      </Link>
    </Slide>
    <Slide>
      <Heading>1. Focus on High Leverage Activities</Heading>
      <Image src="images/leverage.svg"  />
    </Slide>
    <Slide>
      <Heading>2. Optimize for Learning</Heading>
      <Image src="images/books.svg"  />
    </Slide>
    <Slide>
      <Heading>2. Optimize for Learning</Heading>
      <Image src="images/exponential.svg"  />
    </Slide>
    <Slide>
      <Heading>3. Prioritize Regularly</Heading>
      <Image src="images/balance.svg"  />
    </Slide>
    <Slide>
      <Heading>3. Prioritize Regularly</Heading>
      <Table borderCollapse="collapse">
        <TableBody>
          <TableRow>
            <TableCell></TableCell>
            <TableCell textAlign="center">Urgent</TableCell>
            <TableCell textAlign="center">Not Urgent</TableCell>
          </TableRow>
          <TableRow>
            <TableCell textAlign="right">Important</TableCell>
            <TableCell border="solid 1px white">
              Crisis<br/>
              Pressing Issues<br/>
              Deadlines
            </TableCell>
            <TableCell border="solid 1px white">
              Planning and prevention<br/>
              Building relationships<br/>
              New opportunities<br/>
              Personal development
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell textAlign="right">Not Important</TableCell>
            <TableCell border="solid 1px white">
              Interruptions<br/>
              Most meetings<br/>
              Most emails and calls
            </TableCell>
            <TableCell border="solid 1px white">
              Surfing the web<br/>
              Busy work<br/>
              Time wasters
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Slide>
    <Slide>
      <Heading>4. Invest in Iteration Speed</Heading>
      <Image src="images/cycle.svg"  />
    </Slide>
    <Slide>
      <Heading>5. Measure What You Want to Improve</Heading>
      <Image src="images/measure.svg"  />
    </Slide>
    <Slide>
      <Heading>6. Validate Your Ideas Early and Often</Heading>
      <Image src="images/thumbs-up.svg"  />
    </Slide>
    <Slide>
      <Heading>7. Improve Project Estimation Skills</Heading>
      <Image src="images/estimates.png"  />
    </Slide>
    <Slide>
      <Heading>8. Balance Quality With Pragmatism</Heading>
      <Image src="images/stamp-of-approval.svg"  />
    </Slide>
    <Slide>
      <Heading>8. Balance Quality With Pragmatism</Heading>
      <FlexBox>
        <Image width="80%" src="images/ward-cunningham.jpg"  />
        <Box>
          <Text fontStyle="italic">
            Shipping first time code is like going into debt. A little debt speeds development so long as it is paid back promptly…The danger occurs when the debt is not repaid.
          </Text>
          <Text textAlign="right">~ Ward Cunningham</Text>
        </Box>
      </FlexBox>
    </Slide>
    <Slide>
      <Heading>9. Minimize Operational Burden</Heading>
      <Image src="images/rube-goldberg.png"  />
    </Slide>
    <Slide>
      <Heading>9. Minimize Operational Burden</Heading>
      <FlexBox>
        <Image width="80%" src="images/steve-jobs.jpg"  />
        <Box>
          <Text fontStyle="italic">
            …the first solutions you come up with are very complex, and most people stop there…you can oftentimes arrive at some very elegant and simple solutions.
          </Text>
          <Text textAlign="right">~ Steve Jobs</Text>
        </Box>
      </FlexBox>
    </Slide>
    <Slide>
      <Heading>10. Invest in Your Team’s Growth</Heading>
      <Image src="images/team.svg"  />
    </Slide>
    {/* <Slide>
      <Heading>Summary</Heading>
      <Box className="summary">
        <OrderedList fontSize={40}>
          <ListItem>Focus on High-Leverage Activities</ListItem>
          <ListItem>Optimize for Learning</ListItem>
          <ListItem>Prioritize Regularly</ListItem>
          <ListItem>Invest in Iteration Speed</ListItem>
          <ListItem>Measure What You Want to Improve</ListItem>
          <ListItem>Validate Your Ideas Early and Often</ListItem>
          <ListItem>Improve Your Project Estimation Skills</ListItem>
          <ListItem>Balance Quality with Pragmatism</ListItem>
          <ListItem>Minimize Operational Burden</ListItem>
          <ListItem>Invest in Your Team’s Growth</ListItem>
        </OrderedList>
      </Box>
    </Slide> */}
    <Slide>
      <Heading>Discuss</Heading>
      <FlexBox height="100%" alignItems="center" justifyContent="center">
        <Link target="_blank" href="https://gitlab.com/dahjelle/the-effective-engineer-review-2021-02-11/-/blob/main/src/The%20Effective%20Engineer%20Book%20Review%20and%20Summary.md">Notes</Link>
      </FlexBox>
    </Slide>
  </Deck>
);

ReactDOM.render(<Presentation />, document.getElementById('root'));
