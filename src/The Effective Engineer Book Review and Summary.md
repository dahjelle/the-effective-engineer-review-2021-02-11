# The Effective Engineer: Book Summary

**Book:** http://www.effectiveengineer.com/

**Slides:** https://dahjelle.gitlab.io/the-effective-engineer-review-2021-02-11/

**(These) Notes:** https://gitlab.com/dahjelle/the-effective-engineer-review-2021-02-11/-/blob/main/src/The%20Effective%20Engineer%20Book%20Review%20and%20Summary.md

**Repo:** https://gitlab.com/dahjelle/the-effective-engineer-review-2021-02-11

## Introduction

I'm David Hjelle. I've been programming professionally for most of my career, with over ten years at Icon Systems in Moorhead. We're a small company that does church software — keeping track of people, money, donations, and accounting for churches. 

---

## The Effective Engineer

I'm going to talk through the main points of the book _The Effective Engineer_, by Edmond Lau. This book is his attempt at distilling lessons he's learned about what the most effective engineers do — not just "work more hours". I'm no expert — part of the reason I decided to do this talk is to help me review this book's ideas again!

My goal is to pull out some — not all! — of his advice in each of the ten chapters. There's a lot of material here, and it will come pretty rapid-fire, so let's get going!

---

## 1. Focus on High-Leverage Activities

Lau starts by introducing the concept of *leverage*: $\textrm{leverage} = \frac{\textrm{impact produced}}{\textrm{time required}}$. To be most effective, you want your activities to be the highest leverage possible. This is not just accomplishing "easy wins". Good hiring, effective tooling, and developer education are all long-term projects that are high-leverage activities.

You can only increase leverage by:

1. reducing the time an activity takes
2. increasing the output of an activity
3. doing higher-leverage activities instead

The rest of this presentation recommends ways that you can increase the leverage of what you do.

---

## 2. Optimize for Learning

Lau says learning is a high-leverage activity, so you should dedicate time to develop new skills.

---

Why is learning a high-leverage activity? Learning compounds exponentially, like interest. Every new thing you learn enables you to learn other new things that you wouldn't have been able to before.

---

## 3. Prioritize Regularly

Prioritizing is high leverage, because doing one thing means that you _aren't_ doing something else. Some tips to help you prioritize well:

- prioritize tasks with higher leverage
- write all your tasks down — you can't keep them in your head
- limit multitasking
- instead of prioritizing 100s of items: prioritize goals, and then compare leverage between first tasks for those goals

---

Finally, this chart is "the Eisenhower matrix", from the famous general. You've likely seen a variation of it before. Lau points out that you want to make sure you focus on the important and non-urgent. If you spend too much time fighting fires, it's a good sign you haven't been thinking far enough ahead.

---

## 4. Invest in Iteration Speed

You can decrease the time something takes by increasing your iteration speed, the time between creating a change and finding out if it works. Lau has several recommendations. 

First, use **continuous deployment** to quickly release small changes to production. With small changes, bugs tend to be easier to find and any problems tend to be limited. You get feedback much more quickly.

Second, **utilize good tooling** such as hot reloads, linters, static analyzers, faster builds, and more. They will increase not only _your_ development speed, but the whole team's.

Third, **shorten debug loops**. Localize a problem as much as possible. Change your code to focus on the problem at hand. Don't waste time manually setting up a 25-click test.

Finally, **don't ignore non-technical bottlenecks** — sometimes you have to fix people problems, too!

---

## 5. Measure What You Want to Improve

Metrics are a really useful tool to evaluate your progress. However, it is really important to pick the right metric. Optimizing the wrong one often makes things worse. Good metrics are described by:

1. maximum impact (improving the metric really makes a difference)
2. actionable (you know what to do when it goes awry)
3. responsive (know quickly when something changes)
4. robust (not too much noise)

On the flip side, while you only want to _measure_ a handful of things, you want to *instrument* as much as possible so you have visibility for debugging network bandwidth, CPU usage, disk space, etc.

Finally, always be skeptical about your data (so you don't make bad decisions).

---

## 6. Validate Your Ideas Early and Often

The numerator of the leverage equation is "impact produced". You don't know the future, so you must estimate it. Find low-effort ways to validate your work (mock-ups, drawings, write-ups, etc.). That way, you can get buy-in and feedback early. Make sure you listen to it! In fact, build **feedback loops** so that getting feedback is as painless as possible.

---

## 7. Improve Your Project Estimation Skills

The denominator of the leverage equation is "time required". Again, you must estimate. This is a _huge_ topic, but Lau's pointers include:

- decompose large projects into small tasks before estimating
- use multiple approaches for estimation
- don't use wishful thinking
- use clear, specific goals with measurable milestones

Finally, to improve your chances of an accurate estimate, reduce risk early by tackling riskiest and hardest parts first.

---

## 8. Balance Quality with Pragmatism

Balance the need for high-quality code with the need to be able to move as fast as possible. Some of Lau's recommendations include:

**Establish a sustainable code review process.** Code review is a fantastic way of catching bugs, but it is also a great way to share knowledge with a team. But be practical: not every change is going to require the same level of review (if any!).

**Write libraries.** If you can successfully create a library that simplifies a common task, do it. But be aware that it is always harder to build a general solution!

---

Finally, **repay technical debt**. What's technical debt? It is any shortcut you take in order to get code out the door now that you end up paying for later. It's fine to have some technical debt in some areas, but make sure to schedule time to pay off the parts that are slowing you down the most!

>  "Shipping first time code is like going into debt. A little debt speeds development so long as it is paid back promptly…The danger occurs when the debt is not repaid." ~ Ward Cunningham

---

## 9. Minimize Operational Burden

The last thing you want to do is spend time operating your application rather than things that actually make an impact. Finding simple solutions to your problems can help.

---

Some tips to help along the way:

- keep in mind that every additional feature adds complexity
- design to help you debug when something goes wrong
- build systems to fail fast rather than hiding problems
- relentlessly automate mechanical (not decision-making!) tasks

> …the first solutions you come up with are very complex, and most people stop there…you can oftentimes arrive at some very elegant and simple solutions. ~ Steve Jobs

---

## 10. Invest in Your Team's Growth

Finally, you will increase your own effectiveness if you also increase the effectiveness of those around you. There is obviously a lot of opportunity for that in all the previous material, but some additional ideas:

- make hiring everyone's responsibility
- build collective wisdom through postmortems after projects
- invest in educating everyone on your team

---

## Discussion

There is a ton of advice in this book. My advice? Don't try do it all at once. Pick (at most) a couple and revisit regularly. The book definitely has a few hints of "become a great engineer at all costs" — don't forget about your other priorities in life! Similarly, it doesn't address any "self care" tips like "get enough sleep".

That all said — I've really appreciated the idea of _leverage_, and I expect I'll be revisiting this book quite a few times in the future. There are probably better books on any given topic Lau covers, but I've not run into many that cover the breadth of ideas that he does.

What are your thoughts?
